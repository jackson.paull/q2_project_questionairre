{
    "id": "092a1a38-eb47-487d-bd45-cf51beaf7fd9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBGHills_grass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 804,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "abd95ca0-d5c5-471c-b0f4-d6fb71b50f81",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "092a1a38-eb47-487d-bd45-cf51beaf7fd9",
            "compositeImage": {
                "id": "88d9d138-bffe-4b8c-bdf7-bce0a4f47354",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "abd95ca0-d5c5-471c-b0f4-d6fb71b50f81",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8061083e-9db5-4ddd-a529-d4d82982bfb1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "abd95ca0-d5c5-471c-b0f4-d6fb71b50f81",
                    "LayerId": "9c3cb77e-91f0-4eae-bc88-ed86b7a8064e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 805,
    "layers": [
        {
            "id": "9c3cb77e-91f0-4eae-bc88-ed86b7a8064e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "092a1a38-eb47-487d-bd45-cf51beaf7fd9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}