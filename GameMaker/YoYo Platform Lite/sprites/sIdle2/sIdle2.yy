{
    "id": "b737175e-238a-44b8-9bd8-149505ce19ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sIdle2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 122,
    "bbox_left": 0,
    "bbox_right": 72,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "626feadf-8481-4e03-b398-d326c61f6c68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b737175e-238a-44b8-9bd8-149505ce19ae",
            "compositeImage": {
                "id": "f7b1f850-0610-4891-9b6a-67c8c41accca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "626feadf-8481-4e03-b398-d326c61f6c68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66aedafe-38b7-45ca-857e-68eb8fd5d9a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "626feadf-8481-4e03-b398-d326c61f6c68",
                    "LayerId": "a9ada2ef-acd1-4b51-9d07-28bab2be371b"
                }
            ]
        },
        {
            "id": "fed1a429-d89e-47c4-854a-f7f36516ca75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b737175e-238a-44b8-9bd8-149505ce19ae",
            "compositeImage": {
                "id": "eda7e27e-d057-44f7-8d74-179cbc5f0391",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fed1a429-d89e-47c4-854a-f7f36516ca75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62f8ee48-63a4-48e9-9ca2-1350d5c80755",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fed1a429-d89e-47c4-854a-f7f36516ca75",
                    "LayerId": "a9ada2ef-acd1-4b51-9d07-28bab2be371b"
                }
            ]
        },
        {
            "id": "f5485aa6-2028-47a7-92a6-86280e8be586",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b737175e-238a-44b8-9bd8-149505ce19ae",
            "compositeImage": {
                "id": "a79b41a5-b466-480c-b770-cb1f73a9d45f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5485aa6-2028-47a7-92a6-86280e8be586",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "879fa74a-1b14-441d-b22a-9020fc4cfcf9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5485aa6-2028-47a7-92a6-86280e8be586",
                    "LayerId": "a9ada2ef-acd1-4b51-9d07-28bab2be371b"
                }
            ]
        },
        {
            "id": "3c7e0333-72aa-4c2c-b880-aeb3a92c6e56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b737175e-238a-44b8-9bd8-149505ce19ae",
            "compositeImage": {
                "id": "30e1e3fd-a789-4ef3-9160-56026fcd962d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c7e0333-72aa-4c2c-b880-aeb3a92c6e56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acf04a00-6389-4457-8470-115af0243b59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c7e0333-72aa-4c2c-b880-aeb3a92c6e56",
                    "LayerId": "a9ada2ef-acd1-4b51-9d07-28bab2be371b"
                }
            ]
        },
        {
            "id": "e2df41d9-ebd1-403e-b273-429048a5c919",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b737175e-238a-44b8-9bd8-149505ce19ae",
            "compositeImage": {
                "id": "d0242cc5-ee41-4f27-a60d-c739d4f1bc1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2df41d9-ebd1-403e-b273-429048a5c919",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f78275a9-f4bb-4425-b843-cc79b742a84c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2df41d9-ebd1-403e-b273-429048a5c919",
                    "LayerId": "a9ada2ef-acd1-4b51-9d07-28bab2be371b"
                }
            ]
        },
        {
            "id": "7cbc75ac-dd5f-40ff-a14a-c912b6409a0c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b737175e-238a-44b8-9bd8-149505ce19ae",
            "compositeImage": {
                "id": "6411b371-d71c-408d-989d-26247ea654c8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cbc75ac-dd5f-40ff-a14a-c912b6409a0c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83305d90-9f02-4cc6-b36c-8a210af0775d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cbc75ac-dd5f-40ff-a14a-c912b6409a0c",
                    "LayerId": "a9ada2ef-acd1-4b51-9d07-28bab2be371b"
                }
            ]
        },
        {
            "id": "e0e46654-ba97-4372-a0c5-23f2c8dcfddb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b737175e-238a-44b8-9bd8-149505ce19ae",
            "compositeImage": {
                "id": "bb8d9313-be02-42a5-b00b-bb10846a3cd5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0e46654-ba97-4372-a0c5-23f2c8dcfddb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3c82748-719a-4a8a-aaba-a0faa642af4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0e46654-ba97-4372-a0c5-23f2c8dcfddb",
                    "LayerId": "a9ada2ef-acd1-4b51-9d07-28bab2be371b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 123,
    "layers": [
        {
            "id": "a9ada2ef-acd1-4b51-9d07-28bab2be371b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b737175e-238a-44b8-9bd8-149505ce19ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 73,
    "xorig": 36,
    "yorig": 122
}