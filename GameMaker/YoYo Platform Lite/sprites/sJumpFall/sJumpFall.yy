{
    "id": "a44b08eb-fae1-4ec8-8120-3970282ad575",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sJumpFall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 116,
    "bbox_left": 0,
    "bbox_right": 98,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6aadfec-92db-4867-b079-2e942a50324f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a44b08eb-fae1-4ec8-8120-3970282ad575",
            "compositeImage": {
                "id": "5a42079e-a71a-4888-b532-9630f709468c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6aadfec-92db-4867-b079-2e942a50324f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae743c58-7198-4b07-a033-48d7b4a5b34d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6aadfec-92db-4867-b079-2e942a50324f",
                    "LayerId": "b98ad9a3-570d-429f-a8e2-ac96a4af4da8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 117,
    "layers": [
        {
            "id": "b98ad9a3-570d-429f-a8e2-ac96a4af4da8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a44b08eb-fae1-4ec8-8120-3970282ad575",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 99,
    "xorig": 49,
    "yorig": 126
}