{
    "id": "cf50ea67-ff40-44e4-a1fd-8c90cbdfc887",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sClimb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 120,
    "bbox_left": 0,
    "bbox_right": 62,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1817c50e-faec-4cae-b40c-9b5c64e827d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf50ea67-ff40-44e4-a1fd-8c90cbdfc887",
            "compositeImage": {
                "id": "64dca4b2-9321-4657-a580-b4c47b686393",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1817c50e-faec-4cae-b40c-9b5c64e827d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad227bb5-37b0-4c66-955a-ef24e6a7d7e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1817c50e-faec-4cae-b40c-9b5c64e827d7",
                    "LayerId": "2a3b258e-feef-41cd-98fc-9fe694501393"
                }
            ]
        },
        {
            "id": "cadd5066-96ce-421d-9eac-a44bb937e6d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf50ea67-ff40-44e4-a1fd-8c90cbdfc887",
            "compositeImage": {
                "id": "ad2a75c7-3f60-4366-9c19-770726cabeaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cadd5066-96ce-421d-9eac-a44bb937e6d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "80545962-b3bd-45f2-98b3-5e0f9c05b1b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cadd5066-96ce-421d-9eac-a44bb937e6d4",
                    "LayerId": "2a3b258e-feef-41cd-98fc-9fe694501393"
                }
            ]
        },
        {
            "id": "05b8a9a1-dbc8-49a5-be48-949b96a758ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf50ea67-ff40-44e4-a1fd-8c90cbdfc887",
            "compositeImage": {
                "id": "9c54b14a-b327-4b1d-b6d6-67e603069e64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05b8a9a1-dbc8-49a5-be48-949b96a758ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "72e1b965-be13-40b0-ac50-99f8faeda58d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05b8a9a1-dbc8-49a5-be48-949b96a758ea",
                    "LayerId": "2a3b258e-feef-41cd-98fc-9fe694501393"
                }
            ]
        },
        {
            "id": "102abc64-967f-42ef-920e-928b03f11e88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cf50ea67-ff40-44e4-a1fd-8c90cbdfc887",
            "compositeImage": {
                "id": "6032b97c-7e70-44c3-b518-caa76a7e83db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "102abc64-967f-42ef-920e-928b03f11e88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76721c7e-6e06-46fe-aaf0-86899a4493c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "102abc64-967f-42ef-920e-928b03f11e88",
                    "LayerId": "2a3b258e-feef-41cd-98fc-9fe694501393"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 121,
    "layers": [
        {
            "id": "2a3b258e-feef-41cd-98fc-9fe694501393",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cf50ea67-ff40-44e4-a1fd-8c90cbdfc887",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 63,
    "xorig": 31,
    "yorig": 120
}