{
    "id": "71904c12-72ec-4925-b97c-13367f675ae4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBGHills_sand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 804,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "351fa0df-2fe8-4c22-b538-1fb0000a336c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71904c12-72ec-4925-b97c-13367f675ae4",
            "compositeImage": {
                "id": "ae3fff95-7323-4b6c-89cd-f2fc5456be10",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "351fa0df-2fe8-4c22-b538-1fb0000a336c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62dd0521-6690-4423-b51c-9cedbf8f93b1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "351fa0df-2fe8-4c22-b538-1fb0000a336c",
                    "LayerId": "e3dd5c2d-5119-4326-a194-f7be47281a82"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 805,
    "layers": [
        {
            "id": "e3dd5c2d-5119-4326-a194-f7be47281a82",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "71904c12-72ec-4925-b97c-13367f675ae4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}