{
    "id": "613d32c7-4d85-4e4f-b91f-297b941d8147",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCloud_grass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 224,
    "bbox_left": 0,
    "bbox_right": 710,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7572d465-61cb-4591-b04e-24e9f71fcf4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "613d32c7-4d85-4e4f-b91f-297b941d8147",
            "compositeImage": {
                "id": "521c8d4c-f363-46c5-ab49-0bcb2813fc3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7572d465-61cb-4591-b04e-24e9f71fcf4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "76044350-3627-4285-a0e0-25e496e1484d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7572d465-61cb-4591-b04e-24e9f71fcf4f",
                    "LayerId": "ec0107c0-db77-44e3-9ffc-4ec8b7bb2dba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 225,
    "layers": [
        {
            "id": "ec0107c0-db77-44e3-9ffc-4ec8b7bb2dba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "613d32c7-4d85-4e4f-b91f-297b941d8147",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 711,
    "xorig": 0,
    "yorig": 0
}