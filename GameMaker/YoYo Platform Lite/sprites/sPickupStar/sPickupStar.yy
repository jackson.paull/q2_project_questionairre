{
    "id": "fe26fbdf-da1f-481a-81c5-a2ad3dd5bbb7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sPickupStar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 65,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e54cc4cc-07e4-406d-953f-cc95a3dc308e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe26fbdf-da1f-481a-81c5-a2ad3dd5bbb7",
            "compositeImage": {
                "id": "30e71cf3-5e59-4f10-9e41-cf339224d49c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e54cc4cc-07e4-406d-953f-cc95a3dc308e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0807138-d9e9-41eb-8503-c3d978ee073d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e54cc4cc-07e4-406d-953f-cc95a3dc308e",
                    "LayerId": "f93549df-e8bf-4ef8-abc8-324c84a799ea"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f93549df-e8bf-4ef8-abc8-324c84a799ea",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe26fbdf-da1f-481a-81c5-a2ad3dd5bbb7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 66,
    "xorig": 33,
    "yorig": 32
}