{
    "id": "878c3180-2c32-4c63-a4bf-4233646d8451",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGhost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 67,
    "bbox_left": 0,
    "bbox_right": 61,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c48dfb5b-4816-4395-ab1c-23638b8a05f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "878c3180-2c32-4c63-a4bf-4233646d8451",
            "compositeImage": {
                "id": "caa7ccb3-a67f-4bb5-899e-c9f4e2ad3477",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c48dfb5b-4816-4395-ab1c-23638b8a05f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "37d3bfb8-28d7-4e95-8fab-8fbcf870186c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c48dfb5b-4816-4395-ab1c-23638b8a05f4",
                    "LayerId": "c291552a-8a94-4de5-9db8-cfad9714c35c"
                }
            ]
        },
        {
            "id": "26a3dd91-6cdd-467f-887f-86c896a472c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "878c3180-2c32-4c63-a4bf-4233646d8451",
            "compositeImage": {
                "id": "d11fcd4a-bfce-4803-a09b-c5e751a06aa4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26a3dd91-6cdd-467f-887f-86c896a472c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "798f6198-bff6-4578-a7bd-514913dda3f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26a3dd91-6cdd-467f-887f-86c896a472c8",
                    "LayerId": "c291552a-8a94-4de5-9db8-cfad9714c35c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 68,
    "layers": [
        {
            "id": "c291552a-8a94-4de5-9db8-cfad9714c35c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "878c3180-2c32-4c63-a4bf-4233646d8451",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 62,
    "xorig": 0,
    "yorig": 4
}