{
    "id": "50a28c2b-9363-4e0e-9c11-3faa45f59c30",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCloud1_grass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 224,
    "bbox_left": 0,
    "bbox_right": 710,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "58bdf00c-3a82-4107-b80b-b6d9bba492ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "50a28c2b-9363-4e0e-9c11-3faa45f59c30",
            "compositeImage": {
                "id": "b6b6615a-9f93-473a-8bc3-8c5340961b92",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58bdf00c-3a82-4107-b80b-b6d9bba492ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aae41f7f-3544-4f45-a62a-70670715e34d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58bdf00c-3a82-4107-b80b-b6d9bba492ec",
                    "LayerId": "0011ff41-d0d0-48ec-a89e-2825bc628798"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 225,
    "layers": [
        {
            "id": "0011ff41-d0d0-48ec-a89e-2825bc628798",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "50a28c2b-9363-4e0e-9c11-3faa45f59c30",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 711,
    "xorig": 0,
    "yorig": 0
}