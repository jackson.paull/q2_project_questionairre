{
    "id": "92aa1f37-6804-4f06-86ad-25f52f151803",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sWalk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 125,
    "bbox_left": 0,
    "bbox_right": 90,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9253ebeb-8b06-4ce3-a91e-bdd9a0ac62e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92aa1f37-6804-4f06-86ad-25f52f151803",
            "compositeImage": {
                "id": "f10b5d76-1c7b-4a2a-8cdc-385d3e6b9490",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9253ebeb-8b06-4ce3-a91e-bdd9a0ac62e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1cc2328c-817a-4d30-91ee-21fe0fb60068",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9253ebeb-8b06-4ce3-a91e-bdd9a0ac62e1",
                    "LayerId": "9d8a0954-35e0-490c-947e-4476f60f0cbb"
                }
            ]
        },
        {
            "id": "fcaff2c8-66c7-49a4-b420-63561bc15695",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92aa1f37-6804-4f06-86ad-25f52f151803",
            "compositeImage": {
                "id": "021fde84-3d62-4160-a6e0-2a3ddafcc7d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcaff2c8-66c7-49a4-b420-63561bc15695",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c71298bc-74ce-4503-b666-9ddf8678b7fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcaff2c8-66c7-49a4-b420-63561bc15695",
                    "LayerId": "9d8a0954-35e0-490c-947e-4476f60f0cbb"
                }
            ]
        },
        {
            "id": "b96aff21-4590-4df4-9f95-c8fa5e51a5f6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92aa1f37-6804-4f06-86ad-25f52f151803",
            "compositeImage": {
                "id": "c37b8835-003a-4d1f-844e-e3f35c23d74c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b96aff21-4590-4df4-9f95-c8fa5e51a5f6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "057be12e-dd85-484f-85d3-9b59ead7064f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b96aff21-4590-4df4-9f95-c8fa5e51a5f6",
                    "LayerId": "9d8a0954-35e0-490c-947e-4476f60f0cbb"
                }
            ]
        },
        {
            "id": "3b2b348b-6ace-410f-9baf-da67940c0d43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92aa1f37-6804-4f06-86ad-25f52f151803",
            "compositeImage": {
                "id": "596cf8e9-f360-4164-b801-17fc17bc9688",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b2b348b-6ace-410f-9baf-da67940c0d43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2e848362-ca92-4b74-8bf6-d4c3544f759e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b2b348b-6ace-410f-9baf-da67940c0d43",
                    "LayerId": "9d8a0954-35e0-490c-947e-4476f60f0cbb"
                }
            ]
        },
        {
            "id": "b12c12c6-5130-4267-bdfb-6c79c31897ac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92aa1f37-6804-4f06-86ad-25f52f151803",
            "compositeImage": {
                "id": "281f95de-1fae-4cc3-9684-3c3bf4cdba7e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b12c12c6-5130-4267-bdfb-6c79c31897ac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "05d8336c-a8e6-4268-9e17-ad6d82744507",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b12c12c6-5130-4267-bdfb-6c79c31897ac",
                    "LayerId": "9d8a0954-35e0-490c-947e-4476f60f0cbb"
                }
            ]
        },
        {
            "id": "a250833d-7747-425d-9054-706d927d8d41",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92aa1f37-6804-4f06-86ad-25f52f151803",
            "compositeImage": {
                "id": "3357a1d9-dd66-402d-b14d-c3573db54c6c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a250833d-7747-425d-9054-706d927d8d41",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8c8f463-49f6-448e-9fdd-f98fad9817d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a250833d-7747-425d-9054-706d927d8d41",
                    "LayerId": "9d8a0954-35e0-490c-947e-4476f60f0cbb"
                }
            ]
        },
        {
            "id": "dd87f98b-ceea-4a0f-ab6c-7cdc1268cd65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92aa1f37-6804-4f06-86ad-25f52f151803",
            "compositeImage": {
                "id": "9b39ef17-5e40-478a-86d8-977ba121c13b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd87f98b-ceea-4a0f-ab6c-7cdc1268cd65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21684b6f-10b6-4e2c-b516-2a425b2ffed8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd87f98b-ceea-4a0f-ab6c-7cdc1268cd65",
                    "LayerId": "9d8a0954-35e0-490c-947e-4476f60f0cbb"
                }
            ]
        },
        {
            "id": "dd2aa6fb-634d-4357-93f6-b598ade43fe5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "92aa1f37-6804-4f06-86ad-25f52f151803",
            "compositeImage": {
                "id": "aa1f780b-923b-483e-b343-13460c471709",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd2aa6fb-634d-4357-93f6-b598ade43fe5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9151f193-3d83-4734-a47d-68188ed09b73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd2aa6fb-634d-4357-93f6-b598ade43fe5",
                    "LayerId": "9d8a0954-35e0-490c-947e-4476f60f0cbb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 126,
    "layers": [
        {
            "id": "9d8a0954-35e0-490c-947e-4476f60f0cbb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "92aa1f37-6804-4f06-86ad-25f52f151803",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 91,
    "xorig": 45,
    "yorig": 125
}