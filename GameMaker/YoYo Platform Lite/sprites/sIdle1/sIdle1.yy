{
    "id": "a5ff6c66-c0e4-4d78-afaf-d409a73e3368",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sIdle1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 122,
    "bbox_left": 0,
    "bbox_right": 72,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b995d745-5d31-4007-8142-22d724e6901c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5ff6c66-c0e4-4d78-afaf-d409a73e3368",
            "compositeImage": {
                "id": "8a0ef0c0-1df0-4e82-ae82-7a6faf5fd8b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b995d745-5d31-4007-8142-22d724e6901c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dcba6ce-19fc-4320-9940-1bad244ebdf6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b995d745-5d31-4007-8142-22d724e6901c",
                    "LayerId": "6b1f8e1c-a607-40b0-8759-920a305333ef"
                }
            ]
        },
        {
            "id": "1b8bdba5-1564-4bad-9c77-4a31521f77bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5ff6c66-c0e4-4d78-afaf-d409a73e3368",
            "compositeImage": {
                "id": "8ef7fdf6-305f-4232-b957-a0f405da9a31",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1b8bdba5-1564-4bad-9c77-4a31521f77bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86a680f2-9354-46ce-b3fc-6e04dc7f4bb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1b8bdba5-1564-4bad-9c77-4a31521f77bf",
                    "LayerId": "6b1f8e1c-a607-40b0-8759-920a305333ef"
                }
            ]
        },
        {
            "id": "6606e9aa-aa04-4201-b286-9357c2a2e18d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5ff6c66-c0e4-4d78-afaf-d409a73e3368",
            "compositeImage": {
                "id": "77099f6b-e278-4704-8178-343f9871e316",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6606e9aa-aa04-4201-b286-9357c2a2e18d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca2737f4-bef4-4d31-b32c-4ef1cd25f601",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6606e9aa-aa04-4201-b286-9357c2a2e18d",
                    "LayerId": "6b1f8e1c-a607-40b0-8759-920a305333ef"
                }
            ]
        },
        {
            "id": "7b1f6914-b3e4-43c4-b184-e8517e47f01c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5ff6c66-c0e4-4d78-afaf-d409a73e3368",
            "compositeImage": {
                "id": "3c0b718c-b609-4261-a5bf-cc24a9b34614",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b1f6914-b3e4-43c4-b184-e8517e47f01c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5baae080-68d4-4356-9c55-5cc97f7c2bca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b1f6914-b3e4-43c4-b184-e8517e47f01c",
                    "LayerId": "6b1f8e1c-a607-40b0-8759-920a305333ef"
                }
            ]
        },
        {
            "id": "5e92c1db-ba3b-4468-aaf2-c90246fbe13c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5ff6c66-c0e4-4d78-afaf-d409a73e3368",
            "compositeImage": {
                "id": "d9e310bf-5a6e-4b70-a21e-885d60ab8cbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e92c1db-ba3b-4468-aaf2-c90246fbe13c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b56cff6-6370-40e6-9aa3-29209663761b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e92c1db-ba3b-4468-aaf2-c90246fbe13c",
                    "LayerId": "6b1f8e1c-a607-40b0-8759-920a305333ef"
                }
            ]
        },
        {
            "id": "48314468-c09f-4ad3-aa9a-7bb5fd585dc9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5ff6c66-c0e4-4d78-afaf-d409a73e3368",
            "compositeImage": {
                "id": "52fc1951-0eec-42cc-b400-1c88dc5d6554",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48314468-c09f-4ad3-aa9a-7bb5fd585dc9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79457629-d207-4e7c-bf98-abadbdeec368",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48314468-c09f-4ad3-aa9a-7bb5fd585dc9",
                    "LayerId": "6b1f8e1c-a607-40b0-8759-920a305333ef"
                }
            ]
        },
        {
            "id": "9a838347-b84d-4092-8917-0135ed540518",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a5ff6c66-c0e4-4d78-afaf-d409a73e3368",
            "compositeImage": {
                "id": "a0c61095-fc02-4ac1-937c-c41377bcc784",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a838347-b84d-4092-8917-0135ed540518",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d196bddb-167d-4af0-94de-2a5c76ad6d31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a838347-b84d-4092-8917-0135ed540518",
                    "LayerId": "6b1f8e1c-a607-40b0-8759-920a305333ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 123,
    "layers": [
        {
            "id": "6b1f8e1c-a607-40b0-8759-920a305333ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a5ff6c66-c0e4-4d78-afaf-d409a73e3368",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 73,
    "xorig": 36,
    "yorig": 122
}