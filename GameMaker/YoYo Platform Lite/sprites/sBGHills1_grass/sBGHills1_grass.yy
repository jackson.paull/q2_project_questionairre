{
    "id": "b71fe540-b5af-40bc-9297-c953cdecc364",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBGHills1_grass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 804,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a88521dd-d371-49e8-90ed-bf1fdf413e3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b71fe540-b5af-40bc-9297-c953cdecc364",
            "compositeImage": {
                "id": "96466881-e782-40b0-888e-9613ff1e5ca9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a88521dd-d371-49e8-90ed-bf1fdf413e3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9344275-e163-466a-8118-3d5d1ae091e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a88521dd-d371-49e8-90ed-bf1fdf413e3e",
                    "LayerId": "f4c0118f-45ab-4a6c-9c54-41bbfcc1003e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 805,
    "layers": [
        {
            "id": "f4c0118f-45ab-4a6c-9c54-41bbfcc1003e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b71fe540-b5af-40bc-9297-c953cdecc364",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}