{
    "id": "18a5c5f7-b834-4524-a851-3726d8e41b2d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBGGrad_sand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4bf3d577-0dc9-41c4-9183-b480d271b296",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "18a5c5f7-b834-4524-a851-3726d8e41b2d",
            "compositeImage": {
                "id": "d89ddb7f-08d5-4570-a402-6fcefcc02590",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bf3d577-0dc9-41c4-9183-b480d271b296",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9eaf9ff3-bc4a-43b7-9e67-4d0e0dbfd2da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bf3d577-0dc9-41c4-9183-b480d271b296",
                    "LayerId": "c02ab992-0121-429d-a41d-91ddca8633aa"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "c02ab992-0121-429d-a41d-91ddca8633aa",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "18a5c5f7-b834-4524-a851-3726d8e41b2d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}