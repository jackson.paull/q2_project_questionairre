{
    "id": "a8656ed3-e8a6-49cf-bac0-4001ec91eef9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sFall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 125,
    "bbox_left": 0,
    "bbox_right": 90,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "93e090f3-929d-4b98-a443-11aa15924154",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8656ed3-e8a6-49cf-bac0-4001ec91eef9",
            "compositeImage": {
                "id": "e3570d5f-3872-4a34-ae93-cc8c77101c4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93e090f3-929d-4b98-a443-11aa15924154",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "129c7e73-a086-45e6-981f-7fe944c090fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93e090f3-929d-4b98-a443-11aa15924154",
                    "LayerId": "ca672e29-8052-412a-8e8b-54952d95e2e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 126,
    "layers": [
        {
            "id": "ca672e29-8052-412a-8e8b-54952d95e2e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a8656ed3-e8a6-49cf-bac0-4001ec91eef9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 91,
    "xorig": 45,
    "yorig": 126
}