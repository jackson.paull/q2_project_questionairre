{
    "id": "47e11dd9-78ef-48e9-b36b-bd20422d2993",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCloud_sand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 224,
    "bbox_left": 0,
    "bbox_right": 630,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d95442a7-e4d1-4531-92ba-65deb11148c0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47e11dd9-78ef-48e9-b36b-bd20422d2993",
            "compositeImage": {
                "id": "03020d00-7bff-4e7b-ad6e-75f3ae781b0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d95442a7-e4d1-4531-92ba-65deb11148c0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d2f91621-0707-4b78-ba2d-cacf5ec02676",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d95442a7-e4d1-4531-92ba-65deb11148c0",
                    "LayerId": "20e7bc98-35b8-4b39-8c51-daf0a3c387d0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 225,
    "layers": [
        {
            "id": "20e7bc98-35b8-4b39-8c51-daf0a3c387d0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "47e11dd9-78ef-48e9-b36b-bd20422d2993",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 631,
    "xorig": 0,
    "yorig": 0
}