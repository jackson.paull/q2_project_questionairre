{
    "id": "b46abfb1-4abe-42c2-a5f4-8277ebe1eba4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "72a32835-cb8f-47f3-9cc4-c1c8b2bbfe69",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b46abfb1-4abe-42c2-a5f4-8277ebe1eba4",
            "compositeImage": {
                "id": "81125480-a3b7-40b0-b4d7-ee283349c10b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72a32835-cb8f-47f3-9cc4-c1c8b2bbfe69",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c13e6521-f5ca-438b-b4e7-880536ecaf7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72a32835-cb8f-47f3-9cc4-c1c8b2bbfe69",
                    "LayerId": "11fcf360-26bf-4d22-984b-f27c2627a14c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 283,
    "layers": [
        {
            "id": "11fcf360-26bf-4d22-984b-f27c2627a14c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b46abfb1-4abe-42c2-a5f4-8277ebe1eba4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 860,
    "xorig": 430,
    "yorig": 141
}