{
    "id": "02c99179-6796-42df-9f58-dba40c1af901",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 383,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "84204ad7-a6d0-470b-bb02-5dcb8e79efb4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "02c99179-6796-42df-9f58-dba40c1af901",
            "compositeImage": {
                "id": "ad31823b-d75c-4e83-ad4a-f229ea308dbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "84204ad7-a6d0-470b-bb02-5dcb8e79efb4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b91d07cc-d341-4e51-8a8b-b1d4bc9dba9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "84204ad7-a6d0-470b-bb02-5dcb8e79efb4",
                    "LayerId": "4c2cf93c-af73-4721-9e9b-fecdde90020a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "4c2cf93c-af73-4721-9e9b-fecdde90020a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "02c99179-6796-42df-9f58-dba40c1af901",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 384,
    "xorig": 0,
    "yorig": 0
}