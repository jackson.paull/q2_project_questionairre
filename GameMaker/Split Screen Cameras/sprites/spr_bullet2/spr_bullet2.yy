{
    "id": "12a032bd-57a2-4563-a584-e792a165211b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 64,
    "bbox_left": 53,
    "bbox_right": 68,
    "bbox_top": 50,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 213,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c55bcc4b-9f2a-4b08-ad66-9589569a1a1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12a032bd-57a2-4563-a584-e792a165211b",
            "compositeImage": {
                "id": "63e085af-5185-4841-a795-390ec6e23361",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c55bcc4b-9f2a-4b08-ad66-9589569a1a1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b25fc365-76fa-4dab-b1ed-fdb9fe94e936",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c55bcc4b-9f2a-4b08-ad66-9589569a1a1d",
                    "LayerId": "2c3579bb-28bf-49bf-89bf-9cdb84f5f6cd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 114,
    "layers": [
        {
            "id": "2c3579bb-28bf-49bf-89bf-9cdb84f5f6cd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "12a032bd-57a2-4563-a584-e792a165211b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 57,
    "yorig": 57
}