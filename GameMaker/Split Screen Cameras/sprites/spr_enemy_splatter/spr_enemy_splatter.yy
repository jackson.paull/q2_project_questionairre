{
    "id": "700b759e-25ff-4381-9ce8-ad095c3784c8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_splatter",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 113,
    "bbox_left": 0,
    "bbox_right": 114,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e89e675-0677-43bc-8387-bd1bf1f30c50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "700b759e-25ff-4381-9ce8-ad095c3784c8",
            "compositeImage": {
                "id": "41017f95-2c77-4d41-a7cf-91ef1a953dd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e89e675-0677-43bc-8387-bd1bf1f30c50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2fa5f1bd-212e-4f5b-8f71-19920047f3f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e89e675-0677-43bc-8387-bd1bf1f30c50",
                    "LayerId": "dfc1390a-a077-4ade-8e6c-fda966ec2950"
                }
            ]
        },
        {
            "id": "4b8304d9-7e80-42e6-9416-50f67f9c53f9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "700b759e-25ff-4381-9ce8-ad095c3784c8",
            "compositeImage": {
                "id": "f1150ee3-0887-4032-8c95-51e4e9332673",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b8304d9-7e80-42e6-9416-50f67f9c53f9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "58878d16-a4f6-40f6-8cb6-d9960a854a89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b8304d9-7e80-42e6-9416-50f67f9c53f9",
                    "LayerId": "dfc1390a-a077-4ade-8e6c-fda966ec2950"
                }
            ]
        },
        {
            "id": "348acace-84c9-490a-9b40-9f03a7885f9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "700b759e-25ff-4381-9ce8-ad095c3784c8",
            "compositeImage": {
                "id": "7f8676ff-b47b-4666-bd6b-6a1c8e6b8b51",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "348acace-84c9-490a-9b40-9f03a7885f9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13ecb272-aa48-4f75-8b88-91acfd59cb17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "348acace-84c9-490a-9b40-9f03a7885f9a",
                    "LayerId": "dfc1390a-a077-4ade-8e6c-fda966ec2950"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 114,
    "layers": [
        {
            "id": "dfc1390a-a077-4ade-8e6c-fda966ec2950",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "700b759e-25ff-4381-9ce8-ad095c3784c8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 57,
    "yorig": 57
}