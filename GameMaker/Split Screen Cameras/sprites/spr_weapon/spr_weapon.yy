{
    "id": "c6fde204-181a-4cb9-a3bc-67d7fdd790e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_weapon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 3,
    "bbox_right": 33,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 236,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a98a2e79-5c84-4eb5-9c92-5fc17def994b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6fde204-181a-4cb9-a3bc-67d7fdd790e6",
            "compositeImage": {
                "id": "12708614-d0c5-4b80-aaac-8a9f055395b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a98a2e79-5c84-4eb5-9c92-5fc17def994b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "666b9b56-9997-44cc-b914-fcbadf08b57b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a98a2e79-5c84-4eb5-9c92-5fc17def994b",
                    "LayerId": "4b851252-9771-4a12-b1c1-cadb7a989da2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 42,
    "layers": [
        {
            "id": "4b851252-9771-4a12-b1c1-cadb7a989da2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c6fde204-181a-4cb9-a3bc-67d7fdd790e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 37,
    "xorig": 17,
    "yorig": 15
}