{
    "id": "a546cac9-c9d4-4f9c-8b20-27c18a824b08",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_fragment",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 0,
    "bbox_right": 44,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aaecd2ad-72fe-4524-a7b7-4ca98fe331cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a546cac9-c9d4-4f9c-8b20-27c18a824b08",
            "compositeImage": {
                "id": "cfcde1c8-cc84-4a07-9bed-15390d564163",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aaecd2ad-72fe-4524-a7b7-4ca98fe331cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dfac405a-a4b4-4f1d-b34c-b325b273d4ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aaecd2ad-72fe-4524-a7b7-4ca98fe331cd",
                    "LayerId": "61d371ae-34ca-44d0-a241-04498df18b7a"
                }
            ]
        },
        {
            "id": "cd1596ad-0b73-46af-9ccb-814ed5eae623",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a546cac9-c9d4-4f9c-8b20-27c18a824b08",
            "compositeImage": {
                "id": "f77a78c4-1cdb-4b2f-b912-62d4882c3710",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cd1596ad-0b73-46af-9ccb-814ed5eae623",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9f26c0b7-7ba6-4a10-afe0-0314ce542335",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cd1596ad-0b73-46af-9ccb-814ed5eae623",
                    "LayerId": "61d371ae-34ca-44d0-a241-04498df18b7a"
                }
            ]
        },
        {
            "id": "710f4017-7c44-4c77-9866-8c927f418ec1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a546cac9-c9d4-4f9c-8b20-27c18a824b08",
            "compositeImage": {
                "id": "2df1508b-6cb9-4fbb-9d84-003601a37ac0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "710f4017-7c44-4c77-9866-8c927f418ec1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f9a432d9-97b1-4837-b50e-e382112346e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "710f4017-7c44-4c77-9866-8c927f418ec1",
                    "LayerId": "61d371ae-34ca-44d0-a241-04498df18b7a"
                }
            ]
        },
        {
            "id": "5a6bf552-e984-4e4e-a2e6-cba053590e40",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a546cac9-c9d4-4f9c-8b20-27c18a824b08",
            "compositeImage": {
                "id": "176b8ba1-5b90-4b65-8066-56919ee7c176",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a6bf552-e984-4e4e-a2e6-cba053590e40",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "537052d9-9f25-4b3f-9406-5dcdf58b1740",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a6bf552-e984-4e4e-a2e6-cba053590e40",
                    "LayerId": "61d371ae-34ca-44d0-a241-04498df18b7a"
                }
            ]
        },
        {
            "id": "6aa8811d-abaa-41c8-87c5-c9cb40bb0e6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a546cac9-c9d4-4f9c-8b20-27c18a824b08",
            "compositeImage": {
                "id": "c2608106-252b-4226-acaf-4a63b407fcf3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6aa8811d-abaa-41c8-87c5-c9cb40bb0e6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf7ff15f-634f-4b76-a149-00cb0610e6ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6aa8811d-abaa-41c8-87c5-c9cb40bb0e6f",
                    "LayerId": "61d371ae-34ca-44d0-a241-04498df18b7a"
                }
            ]
        },
        {
            "id": "ee7b2a4c-4775-4d03-bc98-fd42a94185be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a546cac9-c9d4-4f9c-8b20-27c18a824b08",
            "compositeImage": {
                "id": "173b5e8b-9d08-49c2-92ac-60f0c8037ab9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee7b2a4c-4775-4d03-bc98-fd42a94185be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a9fac38-7e89-47ed-bc65-8c6f457144cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee7b2a4c-4775-4d03-bc98-fd42a94185be",
                    "LayerId": "61d371ae-34ca-44d0-a241-04498df18b7a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 45,
    "layers": [
        {
            "id": "61d371ae-34ca-44d0-a241-04498df18b7a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a546cac9-c9d4-4f9c-8b20-27c18a824b08",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 20,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 45,
    "xorig": 22,
    "yorig": 22
}