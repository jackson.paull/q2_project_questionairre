{
    "id": "a8b8741a-0b4f-449c-81fd-a3052997d43e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 66,
    "bbox_left": 100,
    "bbox_right": 107,
    "bbox_top": 59,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 255,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7457d864-c33f-4df1-9f36-22d9a69d2899",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8b8741a-0b4f-449c-81fd-a3052997d43e",
            "compositeImage": {
                "id": "c780210f-90ae-4234-893a-fcde293966c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7457d864-c33f-4df1-9f36-22d9a69d2899",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4933a4a-33c1-4fa4-886a-f2d987e5987e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7457d864-c33f-4df1-9f36-22d9a69d2899",
                    "LayerId": "41d65b39-f929-4bf7-9441-c3ef5aae9c67"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "41d65b39-f929-4bf7-9441-c3ef5aae9c67",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a8b8741a-0b4f-449c-81fd-a3052997d43e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}