{
    "id": "e0b093d1-9c66-48c1-b175-9a04bff71657",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_flash",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 113,
    "bbox_left": 0,
    "bbox_right": 114,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "370cb296-da0e-4ed1-9226-db4fc7f3f34f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e0b093d1-9c66-48c1-b175-9a04bff71657",
            "compositeImage": {
                "id": "991139b2-bf06-4238-952e-6614d569a414",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "370cb296-da0e-4ed1-9226-db4fc7f3f34f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee7c365b-b219-450b-b3a9-a74e6e40aeab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "370cb296-da0e-4ed1-9226-db4fc7f3f34f",
                    "LayerId": "1d750569-ccdc-4cdc-95c9-0d645c13f19e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 114,
    "layers": [
        {
            "id": "1d750569-ccdc-4cdc-95c9-0d645c13f19e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e0b093d1-9c66-48c1-b175-9a04bff71657",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 57,
    "yorig": 57
}