{
    "id": "aba626b8-ef74-4371-b7b8-5ce3f04ea05c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 20,
    "bbox_right": 37,
    "bbox_top": 20,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 242,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "127484ef-9577-4d38-974e-d36a56a61cea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aba626b8-ef74-4371-b7b8-5ce3f04ea05c",
            "compositeImage": {
                "id": "d0429dc4-af00-4569-a7d5-cbebbcd176e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "127484ef-9577-4d38-974e-d36a56a61cea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8cc22935-0d92-49b7-bf58-f3ae28421eff",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "127484ef-9577-4d38-974e-d36a56a61cea",
                    "LayerId": "6bf9985e-4da8-47bc-a162-2f45c5c5e82f"
                }
            ]
        },
        {
            "id": "39f1e438-79c5-4c6d-9a08-0f3a73b67ddc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aba626b8-ef74-4371-b7b8-5ce3f04ea05c",
            "compositeImage": {
                "id": "ac98cf61-ee6e-4ffb-822f-9bfc2601c9af",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39f1e438-79c5-4c6d-9a08-0f3a73b67ddc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ccb089d-6377-43a5-9468-bacc9714fa17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39f1e438-79c5-4c6d-9a08-0f3a73b67ddc",
                    "LayerId": "6bf9985e-4da8-47bc-a162-2f45c5c5e82f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "6bf9985e-4da8-47bc-a162-2f45c5c5e82f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aba626b8-ef74-4371-b7b8-5ce3f04ea05c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 57,
    "xorig": 29,
    "yorig": 29
}