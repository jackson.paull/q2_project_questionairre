{
    "id": "9d93ea8f-cd1e-4908-bc3d-484faf2c9971",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_tile",
    "For3D": false,
    "HTile": true,
    "VTile": true,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d85a44a9-6472-4f06-b8b8-f9ae976e79dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9d93ea8f-cd1e-4908-bc3d-484faf2c9971",
            "compositeImage": {
                "id": "dcb673b3-3a3d-4614-9f40-94f12416e78f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d85a44a9-6472-4f06-b8b8-f9ae976e79dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f53b058-4559-434b-b2b4-492ee336b3e4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d85a44a9-6472-4f06-b8b8-f9ae976e79dd",
                    "LayerId": "a3b92f2a-dd14-42e7-8fe9-9bb47530d1c7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "a3b92f2a-dd14-42e7-8fe9-9bb47530d1c7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9d93ea8f-cd1e-4908-bc3d-484faf2c9971",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}