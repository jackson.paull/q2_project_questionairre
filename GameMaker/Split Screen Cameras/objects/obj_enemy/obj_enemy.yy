{
    "id": "628fe2f4-e632-45a5-ae77-8d241feb11c1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_enemy",
    "eventList": [
        {
            "id": "bfd86cc8-211b-4251-8e57-e73643aca0b6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "628fe2f4-e632-45a5-ae77-8d241feb11c1"
        },
        {
            "id": "ecc9d2ac-5753-4df7-aa73-6b8b1ce847d6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "628fe2f4-e632-45a5-ae77-8d241feb11c1"
        },
        {
            "id": "06b95d29-65b7-47d8-96c9-f1dcc9f200f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "628fe2f4-e632-45a5-ae77-8d241feb11c1"
        },
        {
            "id": "38c83b07-c583-4e3a-a20f-5ef16aed235b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "628fe2f4-e632-45a5-ae77-8d241feb11c1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4931a9d0-1049-4819-9b1d-85d88e4064de",
    "visible": true
}