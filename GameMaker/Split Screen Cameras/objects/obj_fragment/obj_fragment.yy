{
    "id": "821a0b3d-0540-4d38-a241-5315071172e8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fragment",
    "eventList": [
        {
            "id": "9d670168-e34f-49d3-b944-33b8283966d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "821a0b3d-0540-4d38-a241-5315071172e8"
        },
        {
            "id": "b688eccb-e488-4522-93d7-2d2bd7b97049",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "821a0b3d-0540-4d38-a241-5315071172e8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a546cac9-c9d4-4f9c-8b20-27c18a824b08",
    "visible": true
}