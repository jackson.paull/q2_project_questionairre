{
    "id": "5760f7ce-3b7a-4f43-910d-31bde1e53675",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_splatter",
    "eventList": [
        {
            "id": "8aadfd57-d0da-4778-bff5-39e813a916ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5760f7ce-3b7a-4f43-910d-31bde1e53675"
        },
        {
            "id": "a0b14b41-71c2-4844-a59d-5629db976f80",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5760f7ce-3b7a-4f43-910d-31bde1e53675"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "700b759e-25ff-4381-9ce8-ad095c3784c8",
    "visible": true
}